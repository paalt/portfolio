export default function(context) {
  if (!context.store.getters['Authentication/isAuthenticated']) {
    context.redirect('/admin/auth');
  }
}
