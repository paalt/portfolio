export class project {
  constructor(id, name, type, year) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.year = year;
  }
}

export class projectDetails extends project {
  super();
  constructor(contractor, duration, role, teamsize, website) {
    this.contractor = contractor;
    this.duration = duration;
    this.role = role;
    this.teamSize = teamsize;
    this.website = website;
  }
}

export class projectImage extends project {
  constructor(coverImageUrl, coverImageClass) {
    this.coverImageUrl = coverImageUrl;
    this.coverImageClass = coverImageClass;
  }
}

export class projectSettings extends project {
  constructor(loadCustomerJourneys) {
    this.loadCustomerJourneys = loadCustomerJourneys;
  }
}

export class projectDetails extends project {
  constructor(areasOfExpertise, methodology, tasks, tools) {
    this.areasOfExpertise = areasOfExpertise;
    this.methodology = methodology;
    this.tasks = tasks;
    this.tools = tools;
  }
}

export class projectContent extends project {
  constructor(projectJSON) {
    this.projectJSON = projectJSON;
  }
}

export class projectSkills extends project {
  constructor(design, innovation, blockchain, development) {
    this.design = design;
    this.innovation = innovation;
    this.blockchain = blockchain;
    this, development = development;
  }
}

export hasKeyHandler = {
  has (target, key) {
    
  }
}