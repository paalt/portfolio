import Vue from 'vue';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEnvelope,
  faToggleOn,
  faToggleOff,
  faCheckSquare,
  faBold,
  faItalic,
  faCode,
  faListUl,
  faListOl,
  faStrikethrough,
  faUnderline,
  faLink,
  faParagraph,
  faHeading,
  faUnlink,
  faImage,
  faTimes
} from '@fortawesome/free-solid-svg-icons';
import { faSquare } from '@fortawesome/free-regular-svg-icons';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faEnvelope);
library.add(faToggleOn);
library.add(faToggleOff);
library.add(faCheckSquare);
library.add(faSquare);
library.add(faBold);
library.add(faItalic);
library.add(faCode);
library.add(faListUl);
library.add(faListOl);
library.add(faStrikethrough);
library.add(faUnderline);
library.add(faLink);
library.add(faParagraph);
library.add(faHeading);
library.add(faUnlink);
library.add(faYoutube);
library.add(faImage);
library.add(faTimes);

Vue.component('fa-icon', FontAwesomeIcon);
