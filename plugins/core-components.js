import Vue from 'vue';

import AppButton from '@/components/UI//Buttons/AppButton';
import AppSwitch from '@/components/UI/Buttons/AppSwitch.vue';
import AppControllerInput from '@/components/UI//Forms/AppControllerInput';
import AppCheckboxInput from '@/components/UI/Forms/AppCheckboxInput.vue';
import AppTabbedNavigation from '@/components/UI/Navigation/AppTabbedNavigation';

Vue.component('AppButton', AppButton);
Vue.component('AppSwitch', AppSwitch);
Vue.component('AppControllerInput', AppControllerInput);
Vue.component('AppCheckboxInput', AppCheckboxInput);
Vue.component('AppTabbedNavigation', AppTabbedNavigation);
