export default {
  inserted: (el, binding) => {
    function loadImage() {
      const imageElement = el;

      let once = {
        once: true
      };

      let error = () => {
        console.log('Error loading image');
      };

      if (imageElement) {
        imageElement.addEventListener(
          'load',
          () => {
            setTimeout(() => el.classList.add('loaded'), 100);
          },
          once
        );
        imageElement.addEventListener('error', error, once);
        imageElement.src = imageElement.dataset.url;
        imageElement.removeEventListener('error', error);
      }
    }

    function handleIntersect(entries, observer) {
      entries.forEach(entry => {
        if (!entry.isIntersecting) {
          return;
        } else {
          loadImage();
          observer.unobserve(el);
        }
      });
    }

    function createObserver() {
      const options = {
        root: null,
        trshold: '0'
      };

      const observer = new IntersectionObserver(handleIntersect, options);

      observer.observe(el);
    }

    if (!('IntersectionObserver' in window)) {
      console.log(
        'This browser does not support IntersectionObserver, lazyload will be disabled and images will load immediately'
      );
      loadImage();
    } else {
      createObserver();
    }
  }
};
