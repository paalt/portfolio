const pkg = require('./package');
const axios = require('axios');

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['~/assets/css/tailwind.css', '~/assets/css/globals.css'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/core-components.js',
    '~plugins/font-awesome.js',
    '~plugins/core-directives.js'
  ],

  router: {
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      }
      if (to.hash) {
        return { selector: to.hash };
      }
      return { x: 0, y: 0 };
    }
  },

  transition: {
    name: 'fade',
    mode: 'out-in'
  },

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.BASE_URL || 'https://portfolio-ddf6e.firebaseio.com'
  },

  env: {
    baseUrl: process.env.BASE_URL || 'https://portfolio-ddf6e.firebaseio.com',
    fbAPIKey: 'AIzaSyCqtgmDCIo0aBEaMHiuDq6OoznDxX3yNDk'
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }
  },
  generate: {
    routes: function() {
      return axios.get('https://portfolio-ddf6e.firebaseio.com/projects.json')
        .then(res => {
          const routes = [];
          for (const key in res.data) {
            routes.push({
              route: '/projects/' + key,
              payload: { projectData: res.data[key] }
            });
          }
          return routes;
        });
    }
  }
};
