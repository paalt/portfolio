import Cookie from 'js-cookie';

export default {
  namespaced: true,
  state: {
    token: null,
    user: '',
    authResponse: {}
  },
  getters: {
    isAuthenticated(state) {
      return state.token != null;
    },
    getActiveUser(state) {
      return state.user;
    },
    getAuthResponse(state) {
      return state.authResponse;
    }
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    clearToken(state) {
      state.token = null;
    },
    setUser(state, payload) {
      state.user = payload;
    },
    clearUser(state) {
      state.user = '';
    },
    setAuthResponse(state, payload) {
      state.authResponse = payload;
    }
  },
  actions: {
    async authenticateUser(vueContext, authData) {
      let authUrl =
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=' +
        process.env.fbAPIKey;

      if (!authData.isLogin) {
        authUrl =
          'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=' +
          process.env.fbAPIKey;
      }

      vueContext.commit('setAuthResponse', {
        message: null
      });

      const authenticate = await this.$axios
        .$post(authUrl, {
          email: authData.email,
          password: authData.password,
          returnSecureToken: true
        })
        .catch(err => {
          vueContext.commit('setAuthResponse', {
            message: err
          });
        });
      const response = vueContext.getters.getAuthResponse;

      if (response.message) {
        if (response.message.response.data.error.code === 400) {
          return;
        }
      }

      vueContext.commit('setToken', authenticate.idToken);
      vueContext.commit('setUser', authData.email);

      localStorage.setItem('token', authenticate.idToken);
      localStorage.setItem(
        'tokenExpiration',
        new Date().getTime() + Number.parseInt(authenticate.expiresIn) * 1000
      );
      localStorage.setItem('user', authData.email);

      Cookie.set('jwt', authenticate.idToken);
      Cookie.set(
        'expirationDate',
        new Date().getTime() + Number.parseInt(authenticate.expiresIn) * 1000
      );
      Cookie.set('user', authData.email);
    },
    initAuth(vueContext, req) {
      let token;
      let expirationDate;
      let user;

      if (req) {
        if (!req.headers.cookie) {
          return;
        }

        const jwtCookie = req.headers.cookie
          .split(';')
          .find(c => c.trim().startsWith('jwt='));

        const userCookie = req.headers.cookie
          .split(';')
          .find(c => c.trim().startsWith('user='));

        if (!jwtCookie || !userCookie) {
          return;
        }

        token = jwtCookie.split('=')[1];
        expirationDate = req.headers.cookie
          .split(';')
          .find(c => c.trim().startsWith('expirationDate='))
          .split('=')[1];
        user = userCookie.split('=')[1];
      } else if (process.client) {
        token = localStorage.getItem('token');
        expirationDate = localStorage.getItem('tokenExpiration');
        user = localStorage.getItem('user');
      }

      if (new Date().getTime() > +expirationDate || !token) {
        vueContext.dispatch('logout');
        return;
      }

      vueContext.commit('setToken', token);
      vueContext.commit('setUser', user);
    },
    logout(vueContext) {
      vueContext.commit('clearToken');
      vueContext.commit('clearUser');

      Cookie.remove('jwt');
      Cookie.remove('expirationDate');
      Cookie.remove('user');

      if (process.client) {
        localStorage.removeItem('token');
        localStorage.removeItem('tokenExpiration');
        localStorage.removeItem('user');
      }
    }
  }
};
