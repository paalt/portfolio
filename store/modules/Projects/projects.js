export default {
  namespaced: true,
  state: {
    loadedProject: {},
    loadedProjects: []
  },
  getters: {
    getLoadedProject(state) {
      return state.loadedProject;
    },
    getLoadedProjects(state) {
      return state.loadedProjects;
    }
  },
  mutations: {
    setLoadedProject(state, payload) {
      state.loadedProject = payload;
    },
    setLoadedProjects(state, payload) {
      state.loadedProjects = payload;
    }
  },
  actions: {
    nuxtServerInit(vuexContext, context) {
      return context.app.$axios
        .get('/projects.json')
        .then(res => {
          const projectsArray = [];
          for (const key in res.data) {
            projectsArray.push({ ...res.data[key], id: key });
          }
          vuexContext.commit('setLoadedProjects', projectsArray);
        })
        .catch(e => context.error(e));
    },
    setLoadedProjects(context) {
      return this.$axios
        .get('/projects.json')
        .then(res => {
          context.commit('setLoadedProjects', res.data);
        })
        .catch(e => context.error(e));
    },
    setLoadedProject({ commit }, payload) {
      commit('setLoadedProject', payload);
    }
  }
};
