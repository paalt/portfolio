export default {
  namespaced: true,
  state: {
    featuredDesign: ''
  },
  getters: {
    getFeaturedDesign(state) {
      return state.featuredDesign;
    }
  },
  mutations: {
    setFeaturedDesign(state, payload) {
      state.featuredDesign = payload;
    }
  },
  actions: {
    setFeaturedDesign({ commit }, payload) {
      commit('setFeaturedDesign', payload);
    }
  }
};
