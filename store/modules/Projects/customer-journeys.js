export default {
  namespaced: true,
  state: {
    loadedCustomerJourneys: {}
  },
  getters: {
    getLoadedCustomerJourneys(state) {
      return state.loadedCustomerJourneys;
    }
  },
  mutations: {
    setLoadedCustomerJourneys(state, payload) {
      state.loadedCustomerJourneys = payload;
    }
  },
  actions: {
    setLoadedCustomerJourneys({ commit }, payload) {
      commit('setLoadedCustomerJourneys', payload);
    }
  }
};
