export default {
  namespaced: true,
  state: {
    activeCategory: 'All'
  },
  getters: {
    getActiveCategory(state) {
      return state.activeCategory;
    }
  },
  mutations: {
    setActiveCategory(state, payload) {
      state.activeCategory = payload;
    }
  },
  actions: {
    setActiveCategory({ commit }, payload) {
      commit('setActiveCategory', payload);
    }
  }
};
