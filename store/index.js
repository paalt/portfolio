import Vuex from 'vuex';
import Projects from '@/store/modules/Projects/projects.js';
import CustomerJourneys from '@/store/modules/Projects/customer-journeys.js';
import FeaturedProjects from '@/store/modules/Projects/featured.js';
import Authentication from '@/store/modules/auth.js';
import Navigation from '@/store/modules/navigation.js';

const createStore = () => {
  return new Vuex.Store({
    modules: {
      Projects,
      CustomerJourneys,
      FeaturedProjects,
      Authentication,
      Navigation
    },
    state: {
      loadedVideos: {}
    },
    getters: {
      getLoadedVideos(state) {
        return state.loadedVideos;
      }
    },
    mutations: {
      setLoadedVideos(state, payload) {
        state.loadedVideoes = payload;
      }
    },
    actions: {
      setLoadedVideos({ commit }, payload) {
        commit('setLoadedVideos', payload);
      }
    }
  });
};

export default createStore;
